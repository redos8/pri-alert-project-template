/**
 * Libraries is here
 */
import $ from 'jquery';
import Backbone from 'backbone';

/**
 * Views is here
 */
import TestView from 'views/TestView';

import '../styles/styles.scss';

$(() => {
    new TestView();
});
