import {TweenLite, TweenMax, TimelineLite, TimelineMax, EasePack, CSSPlugin, RoundPropsPlugin, BezierPlugin, AttrPlugin, DirectionalRotationPlugin} from 'TweenMax';
import ScrollMagic from 'ScrollMagic';
import Backbone from 'backbone';
import { time, throttle, debounce } from 'core-decorators';

export default class BaseView extends Backbone.View {

    constructor(options = {}) {
        options = _.extend({
            el: '.base-content-wrapper',
            events: {
                'click h1': 'append'
            }
        }, options);
        super(options);
    }

    @debounce(500)
    @time('test')
    append() {
        let rand = _.random(0, 1);
        this.$('ul').append(`<li>${rand}</li>`);
    }

    initialize() {}
}

