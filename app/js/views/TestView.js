import { mixin } from 'core-decorators';
import BaseView from 'views/BaseView';
import FragmentView from 'views/FragmentView';

@mixin(FragmentView)
export default class TestView extends BaseView {

    constructor() {
        super({events: {'click .red': 'fragment'}});
    }

}
