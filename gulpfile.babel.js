

import gulp from 'gulp';
import gulpLoadPlugins from 'gulp-load-plugins';
import sass from 'gulp-sass';
import sourcemaps from 'gulp-sourcemaps';
import autoprefixer from 'gulp-autoprefixer';
import browserSync from 'browser-sync';
import fileinclude from 'gulp-file-include';
import webpack from 'webpack-stream';
import htmlInjector from 'bs-html-injector';

const $ = gulpLoadPlugins();
const reload = browserSync.reload;

var bs = browserSync.create();
gulp.task('include', () => {
    return gulp.src('app/*.html')
        .pipe(fileinclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(gulp.dest('.tmp'))


});

gulp.task('include-build', () => {
    return gulp.src('app/*.html')
        .pipe(fileinclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(gulp.dest('build'))
});

gulp.task('webpack:dev', ['include'], () => {
    browserSync({
        notify: false,
        port: 3000,
        ui: false,
        server: {
            baseDir: ['.tmp', 'app'],
        }
    });

    gulp.watch('.tmp/**/*.js').on('change', reload);
    gulp.watch(["app/*.html", "app/html/**/*.html"], ['serve:makeup:include']);
    gulp.watch(["app/*.html", "app/html/**/*.html"], htmlInjector);

    return gulp.src('app/js/*.js')
        .pipe(webpack(require('./webpack.config.js')))
        .pipe(gulp.dest('.tmp/'));
});

gulp.task('webpack:build', ['include-build'], () => {
    return gulp.src('app/js/*.js')
        .pipe(webpack(require('./webpack.config.build.js')))
        .pipe(gulp.dest('build/'));
});

gulp.task('serve:makeup', ['serve:makeup:styles', 'serve:makeup:include'], () => {

    bs.use(htmlInjector, {
        files: ["app/*.html", "app/html/**/*.html"]
    });
    bs.init({
        notify: false,
        port: 3000,
        ui: false,
        server: {
            baseDir: ['.tmp', 'app']
        }
    });

    gulp.watch(["app/*.html", "app/html/**/*.html"], ['serve:makeup:include']);
    gulp.watch(["app/*.html", "app/html/**/*.html"], htmlInjector);
    gulp.watch(["app/styles/*.scss", "app/styles/**/*.scss"], ['serve:makeup:styles']);
});

gulp.task('serve:makeup:styles', () => {
    return gulp.src('app/styles/styles.scss')
        .pipe(sourcemaps.init())
        .pipe(autoprefixer({
            browsers: ['last 4 versions'],
            cascade: false
        }))
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('app/styles'))
        .pipe(bs.stream());
});


gulp.task('serve:makeup:include', () => {
    return gulp.src('app/*.html')
        .pipe(fileinclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(gulp.dest('.tmp'))
        .pipe(bs.stream());
});