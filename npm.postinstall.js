var exec = require('child_process').exec,
    child;
const del = require('del');

exec('bower install', function (error, stdout, stderr) {
    console.log('stdout: ' + stdout);
    console.log('stderr: ' + stderr);

    if (error !== null) {
        console.log('exec error: ' + error);
    }else{
        exec('cd app/styles && bower install', function (error, stdout, stderr) {
            console.log('stdout: ' + stdout);
            console.log('stderr: ' + stderr);

            if (error !== null) {
                console.log('exec error: ' + error);
            }else{
                del.sync(['.tmp']);
                exec('gulp webpack:dev');
            }
        });
    }
});