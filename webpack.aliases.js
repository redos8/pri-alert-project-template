
module.exports = {
    'TweenLite': 'libs/gsap/src/minified/TweenLite.min',
    'TweenMax': 'libs/gsap/src/minified/TweenMax.min',
    'TimelineMax': 'libs/gsap/src/minified/TimelineMax.min',
    'ScrollToPlugin': 'libs/gsap/src/minified/plugins/ScrollToPlugin.min',
    'iscroll': 'libs/iscroll/build/iscroll-probe',
    'scrollmagic': 'libs/scrollmagic/scrollmagic/minified/ScrollMagic.min',
    'ScrollMagic': 'libs/scrollmagic/scrollmagic/minified/ScrollMagic.min',
    'scrollmagicGsap': 'libs/scrollmagic/scrollmagic/minified/plugins/animation.gsap.min',
    'scrollmagicIndicators': 'libs/scrollmagic/scrollmagic/minified/plugins/debug.addIndicators.min',
    'parallax': 'libs/parallax/deploy/parallax'
};