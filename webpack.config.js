'use strict'

const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
import ManifestPlugin from 'webpack-manifest-plugin';
import aliases from './webpack.aliases.js';


module.exports = {
  watch: true,
  'context': path.join(__dirname, '/app'),
  'resolve': {
    modulesDirectories: ["web_modules", "node_modules", "bower_components", "libs"],
    'root': [
      path.join(__dirname, '/app/js')
    ],
    'alias': aliases
  },
  'entry': {
    'babel-polyfill': 'babel-polyfill',
    'bootstrap': 'bootstrap.js'
  },
  'output': {
    // path: __dirname + '/assets',
    filename: '[name].js',
    publicPath: '/'
  },
  devtool: "source-map",
  'module': {
    'loaders': [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components|libs)/,
        loader: 'babel', // 'babel-loader' is also a legal name to reference
        query: {
          cacheDirectory: true,
          plugins: ['transform-decorators-legacy'],
          presets: ['es2015']
        }
      },
      {
        test: /\.css$/,
        exclude: /node_modules/,
        loader: 'style-loader!css-loader!autoprefixer-loader!'
      },
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract("css?sourceMap!autoprefixer!sass?sourceMap!sass-bulk-import")
      },
      {test: /\.png|\.jpg$/, loader: "file?name=[path][name].[ext]"},
      {test: /\.html$/, exclude: /index\.html$/, loader: "html"},
      {test: /\.(ttf|eot|svg|woff|woff2)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file?name=[path][name].[ext]"}
    ]
  },

  sassLoader: {
    includePaths: [path.resolve(__dirname, './app/styles')]
  },

  plugins: [
    new webpack.ProvidePlugin({
      $: "jquery",
      "Backbone": "backbone",
      "_": "underscore"
    }),
    new webpack.ResolverPlugin(
        new webpack.ResolverPlugin.DirectoryDescriptionFilePlugin('package.json', ['main'])
    ),
    new webpack.ResolverPlugin(
        new webpack.ResolverPlugin.DirectoryDescriptionFilePlugin(".bower.json", ["main"])
    ),
    new webpack.optimize.CommonsChunkPlugin('common.js'),
    new ExtractTextPlugin('styles/styles.css', {
      allChunks: true
    }),
    new ManifestPlugin()
  ]
}
